import ContactForm from './components/contact-form';
import Instagram from './components/instagram';
import Layout from './components/layout';

import { GatsbyImage } from 'gatsby-plugin-image';
import React from 'react';
import { Helmet } from 'react-helmet';

import { contactData } from '../graphql/contact';

const contact = () => {
  const data = contactData();
  return (
    <Layout>
      <Helmet>
        <title>Contact - Petra Flucka Photography</title>
        <meta property="og:title" content="Contact - Petra Flucka Photography" />
        <meta property="og:image" content={data.image.src} />
        <meta property="og:url" content="https://petraflucka.com/contact/" />
        <meta property="og:description" content="I can’t wait to hear what you’ve been cooking up." />
      </Helmet>
      <div className="md:flex md:space-x-10 px-0 md:px-20">
        <div className="w-full md:w-2/5 pb-4 md:pb-0">
          <GatsbyImage image={data.image.gatsbyImage} alt="" />
        </div>
        <ContactForm />
      </div>
      <Instagram />
    </Layout>
  );
};

export default contact;
