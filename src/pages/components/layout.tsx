import Footer from './footer';
import Header from './header';

import React from 'react';
import { Helmet } from 'react-helmet';

const Layout: React.FC = ({ children }) => {
  return (
    <div className="flex p-4 md:p-12 justify-center">
      <Helmet>
        <meta charSet="utf-8" />
        <title>Petra Flucka Photography</title>
        <meta property="og:title" content="Petra Flucka Photography" />
        <meta property="og:type" content="website" />
        <meta property="og:image" content="https://petraflucka.com/icon.png" />
        <meta property="og:url" content="https://petraflucka.com/" />
        <meta property="og:description" content="Petra Flucka Food & Lifestyle Photography" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:site_name" content="Petra Flucka Photography" />
      </Helmet>
      <div className="flex flex-col max-w-screen-xl min-h-screen w-full">
        <Header />
        <div className="flex flex-col space-y-12">{children}</div>
        <Footer />
      </div>
    </div>
  );
};

export default Layout;
