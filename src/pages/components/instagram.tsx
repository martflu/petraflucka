import { graphql, useStaticQuery } from 'gatsby';
import { GatsbyImage, IGatsbyImageData } from 'gatsby-plugin-image';
import React, { useState } from 'react';
import { isMobile } from 'react-device-detect';
import { FaInstagram } from 'react-icons/fa';

const Instagram = () => {
  const [hovered, setHovered] = useState(false);

  const { images } = useStaticQuery(graphql`
    query {
      images: allFile(filter: { sourceInstanceName: { eq: "instagram-images" } }, limit: 4, sort: { fields: name }) {
        edges {
          node {
            id
            name
            childImageSharp {
              gatsbyImageData(placeholder: BLURRED)
            }
          }
        }
      }
    }
  `);

  return (
    <div className="px-0 md:px-40">
      <h2 className="text-center pb-2">Follow me on Instagram</h2>
      <a href="https://www.instagram.com/petraflucka" target="_blank" rel="noopener noreferrer">
        <div onMouseOver={() => setHovered(true)} onMouseLeave={() => setHovered(false)}>
          <div className="relative grid md:grid-cols-4 grid-cols-2 w-full cursor-pointer">
            {images.edges.map(
              ({
                node,
              }: {
                node: { id: string; name: string; childImageSharp: { gatsbyImageData: IGatsbyImageData } };
              }) => {
                return <GatsbyImage key={node.id} image={node.childImageSharp.gatsbyImageData} alt="" />;
              },
            )}
            {(hovered || isMobile) && (
              <div className="absolute place-self-center">
                <div className="flex flex-col place-items-center">
                  <div className="text-white">@petraflucka</div>
                  <FaInstagram color="white" size="4em" />
                </div>
              </div>
            )}
          </div>
        </div>
      </a>
    </div>
  );
};

export default Instagram;
