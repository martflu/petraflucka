import { IGatsbyImageData } from 'gatsby-plugin-image';

export interface Image {
  key?: string;
  gatsbyImage: IGatsbyImageData;
  src: string;
  originalName: string;
}
