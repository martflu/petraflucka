## Editorial photography

Whether it’s for a magazine, blog, or website, I focus on the little details — a rose petal here, a dust of powdered sugar there — that bring your editorial shots and narrative to life. From the props and styling to the composition and lighting, I don’t stop until every element is just right.
