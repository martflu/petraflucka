## Cookbook & lifestyle book photography

Take your readers on a journey with photography that brings your recipes and words to life. Highlight a story of slow living with sumptuous shots of fresh produce and natural greenery or transport your audience to an idyllic farm in the Nordic countryside. Whatever your story, I work with you to create a cohesive set of images that keep the pages turning.
