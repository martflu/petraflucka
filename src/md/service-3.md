## Lifestyle photography

Highlight the beauty of life’s simple moments with artistic lifestyle shots that elevate the look and feel of your brand. Whether it’s a close-up of the way someone holds their coffee or a motion shot of a linen tablecloth billowing on a clothesline, I help you capture the ethos of your brand.
