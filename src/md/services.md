### How I work

What idea do you want to transmit? What impression are you trying to get across? Before any shots are taken, I work with you to understand your goals and dreams for the images. From there, I help you translate those desires into a creative direction, finding the right, props, displays, settings, and lighting to bring your vision to life.

Working languages: I work in English and Slovak and speak German conversationally. Location: I’m available for jobs around the globe.
